import aims
from nose.tools import assert_almost_equal

def test_std1(): #floats
    obs = aims.std([0.1,1.0])
    exp = 0.6363961
    assert_almost_equal(obs, exp)

def test_std2(): #positive numbers
    obs = aims.std([1,1])
    exp = 0
    assert_almost_equal(obs, exp)

def test_std3(): #positive and negative
    obs = aims.std([-1,3])
    exp = 2.8284271
    assert_almost_equal(obs, exp)

def test_std4(): #negatives
    obs = aims.std([-1,-1])
    exp = 0
    assert_almost_equal(obs, exp)

def test_avg1():
    files = ['data/bert/audioresult-00317']
    obs = aims.avg_range(files)
    exp = 6.0
    assert_almost_equal(obs, exp)

def test_avg1():
    files = ['data/bert/audioresult-00384']
    obs = aims.avg_range(files)
    exp = 1.0
    assert_almost_equal(obs, exp)

def test_avg1():
    files = ['data/bert/audioresult-00215']
    obs = aims.avg_range(files)
    exp = 5.0
    assert_almost_equal(obs, exp)


