#!/usr/bin/env python
import numpy as np
def mean(numbers):
    try:
       return sum(numbers) / len(numbers)
    except ZeroDivisionError as detail :
        msg = "Cannot take the mean of an empty list of numbers."
        raise ZeroDivisionError(detail.__str__() + "\n" +  msg)

def std(numbers):
    variance = 0
    mn = mean(numbers) #sum(numbers)/float(len(numbers))
    for i in numbers:
        variance += (i - mn) ** 2
        #variance /= len(numbers)
    return np.sqrt(variance)

def avg_range(filenames):
    files = []
    ranges = []
    for location in filenames:
        files.append(open(location))
    for entry in files:
        for line in entry:
            if 'Range' in line:
                ranges.append(float(line[7]))
            
    return sum(ranges) / len(ranges)


def main():
    print std([2,2])
    print "Welcome to the AIMS module"

if __name__ == "__main__":
    main()
